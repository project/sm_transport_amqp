<?php

declare(strict_types=1);

namespace Drupal\sm_transport_amqp;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Service provider for Symfony Messenger AMQP.
 */
final class SmTransportAmqpServiceProvider implements ServiceProviderInterface
{

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container)
  {
    $container
      // Run before 'Senders Service Locator' is built in
      // SymfonyMessengerCompilerPass (priority: 0).
      ->addCompilerPass(new SmTransportAmqpCompilerPass(), priority: 100);
  }
}
