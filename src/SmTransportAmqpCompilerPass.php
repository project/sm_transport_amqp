<?php

declare(strict_types=1);

namespace Drupal\sm_transport_amqp;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Symfony Messenger Amqp compiler pass.
 */
final class SmTransportAmqpCompilerPass implements CompilerPassInterface
{

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void
  {
    $transports = $container->getParameter('sm_transport_amqp.transports');

    // Similar to symfony/framework-bundle/DependencyInjection/FrameworkExtension.php:2192
    // Transports would normally be transformed from YAML to services.
    foreach ($transports as $name => $transport) {
      $serializerId = $transport['serializer'] ?? 'messenger.default_serializer';
      $transportDefinition = (new Definition(TransportInterface::class))
        ->setFactory([new Reference('messenger.transport_factory'), 'createTransport'])
        ->setArguments([
          $transport['dsn'],
          $transport['options'] + ['transport_name' => $name],
          new Reference($serializerId),
        ])
        ->addTag('messenger.receiver', [
          'alias' => $name,
          'is_failure_transport' => FALSE,
        ]);
      $container->setDefinition('sm.transport.' . $name, $transportDefinition);
    }
  }
}
